package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;

public class Collections {
    public static void main (String[] args){

        // Arrays are fixed-size data structures that store a collection of elements of the same type

        int[] intArray = new int[5];
        System.out.println("Initial state of intArray: ");
        System.out.println(Arrays.toString(intArray));
        //  0  1  2  3  4   5
        // [0, 0, 0, 0, 0]
        intArray[0] = 1;
        intArray[intArray.length-1] = 5;
        System.out.println("Updated Array: ");
        System.out.println(Arrays.toString(intArray));
        // Could not add elements because the length is already fixed.
//        intArray[intArray.length] = 6;
//        System.out.println(Arrays.toString(intArray));

        // Multidimensional arrays
        // 3 rows, 3 columns
        String[][] classroom = new String[3][3];

        // First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";

        // Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Junjun";
        classroom[1][2] = "Jobert";

        // Third Row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.deepToString(classroom));

        // ArrayLists are dynamic data structures that can grow or shrink in size as needed.
        System.out.println("----------------");
        ArrayList<String> students = new ArrayList<String>();
        System.out.print(students);
        students.add("marvin");
        students.add("jerome");

        System.out.println(students.size());
        System.out.println(students.get(0));
        System.out.println(students.get(1));
        System.out.println(students);

        students.set(1, "king");
        System.out.println(students);
    }
}
